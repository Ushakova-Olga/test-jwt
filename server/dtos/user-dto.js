module.exports = class UserDto {
    // data transfer object
    email;
    id;
    isActivated;

    constructor(model) {
        this.email = model.email;
        this.id = model._id; // бд mongo добавляет нижнее подчеркивание
        this.isActivated = model.isActivated;
    }
}