const { Schema, model } = require('mongoose');

/* const userSchema = new Schema({
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    isActivated: { type: Boolean, default: false },
    activationLink: { type: String, required: true },
});*/
class userSchema {
    email;
    password;
    isActivated;
    activationLink;

    constructor(
        email,
        password,
        isActivated,
        activationLink,
    ) {
        this.activationLink = activationLink;
        this.email = email;
        this.isActivated = isActivated;
        this.password = password;
    }
}


module.exports = userSchema; // model('User', userSchema);